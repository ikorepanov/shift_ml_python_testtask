"""
database.py

This module provides functionality for connecting to a SQLite database using
SQLAlchemy. Most of the code used in the module is standard SQLAlchemy code.

After running the program the file named 'db.sqlite3' will be located
in the main directory of the project.

SQLAlchemy "engine" will be used in different places during
the program's execution.

Each instance of the SessionLocal class represents a database session.

The declarative_base function is used to create a base class that will be
inherited by each of the database models or classes (the ORM models).
"""

from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base, sessionmaker

SQLALCHEMY_DATABASE_URL = 'sqlite:///./db.sqlite3'

engine = create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={'check_same_thread': False}
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
