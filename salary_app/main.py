"""
main.py

This module contains the main FastAPI application for the Salary App.
The application provides routes for user registration, login, and accessing
user information.
"""

from datetime import timedelta
from typing import Annotated

from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

from salary_app import crud, deps, models, schemas, utils
from salary_app.database import engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI(
    title='Salary App'
)


@app.post('/signup',
          summary='Register a new user (employee)',
          response_model=schemas.User)
def create_user(user: schemas.UserCreate,
                db: Session = Depends(deps.get_db)):
    """Creates a new user."""
    db_user = crud.get_user_by_username(db, username=user.username)
    if db_user:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail='Username already registered')
    return crud.create_user(db=db, user=user)


@app.post('/login',
          summary='Log in, get a token by username and password',
          response_model=schemas.Token)
def login_for_access_token(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
    db: Session = Depends(deps.get_db)
):
    """Obtains an access token for a user."""
    user = utils.authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Incorrect username or password',
            headers={'WWW-Authenticate': 'Bearer'},
        )
    access_token_expires = timedelta(
        minutes=int(utils.ACCESS_TOKEN_EXPIRE_MINUTES)
    )
    access_token = utils.create_access_token(
        data={'sub': user.username}, expires_delta=access_token_expires
    )
    return {'access_token': access_token, 'token_type': 'bearer'}


@app.get('/users/me',
         summary=('View personal data including current salary and date of '
                  'the next increase'),
         response_model=schemas.User)
def read_users_me(
    current_user: Annotated[schemas.User, Depends(deps.get_current_user)]
):
    """Gets the current user's information."""
    return current_user
