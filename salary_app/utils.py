"""
utils.py

This module provides utility functions for the Salary App, including
password hashing, authentication, and token generation.
"""

import os
from datetime import datetime, timedelta
from typing import Optional, Union

from dotenv import load_dotenv
from fastapi.security import OAuth2PasswordBearer
from jose import jwt
from passlib.context import CryptContext
from sqlalchemy.orm import Session

from salary_app import crud, schemas

load_dotenv()

SECRET_KEY: Optional[str] = os.getenv('SECRET_KEY')
ALGORITHM: Optional[str] = os.getenv('ALGORITHM')
ACCESS_TOKEN_EXPIRE_MINUTES: Optional[str] = os.getenv(
    'ACCESS_TOKEN_EXPIRE_MINUTES'
    )

pwd_context = CryptContext(schemes=['bcrypt'], deprecated='auto')

oauth2_scheme = OAuth2PasswordBearer(tokenUrl='login')


def get_password_hash(password: str) -> str:
    """
    Hashes the provided password coming from the user using the passlib
    context.
    """
    return pwd_context.hash(password)


def verify_password(plain_password: str, hashed_password: str) -> bool:
    """Verifies if the provided plain password matches the hashed password."""
    return pwd_context.verify(plain_password, hashed_password)


def authenticate_user(db: Session,
                      username: str,
                      password: str) -> Union[schemas.User, bool]:
    """
    Authenticates and return a user by verifying the provided username
    and password.
    """
    user = crud.get_user_by_username(db, username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


def create_access_token(data: dict,
                        expires_delta: Union[timedelta, None] = None) -> str:
    """Generates a new access token."""
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({'exp': expire})
    return jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
