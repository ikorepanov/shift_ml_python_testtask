"""
config.py

This module provides a class for managing application settings and
configuration. It defines a `Settings` class that inherits from `BaseSettings`
from the `pydantic` library.
"""

from pydantic import BaseSettings


class Settings(BaseSettings):
    """
    Class encapsulates the configuration variables required by the application.
    """

    SECRET_KEY: str
    ALGORITHM: str
    ACCESS_TOKEN_EXPIRE_MINUTES: str

    class Config:
        env_file = ".env"
