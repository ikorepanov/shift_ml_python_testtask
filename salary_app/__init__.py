"""
Salary_app Package

This package contains modules related to the Salary App.

Modules:
- main: contains the main application logic;
- crud: provides functions for CRUD operations;
- database: handles the database connection and setup;
- deps: defines dependency functions for FastAPI;
- models: defines the database models;
- schemas: defines the Pydantic models for request/response validation;
- utils: contains utility functions;
- config: stores configuration settings for the application;
- tests: package containing unit tests for the application.

Usage:
Import modules from the package as needed, e.g.:
    from salary_app import utils
    from salary_app import crud

Author:
Ilia Korepanov

Contact:
Telegram: @number_one_lobster
"""
