"""
models.py

This module contains the database models for the application. It defines
the SQLAlchemy model classes that represent the tables in the database.
"""

from sqlalchemy import Column, Date, Integer, String

from salary_app.database import Base


class User(Base):
    """
    User model representing the 'users' table in the database.
    """

    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    current_salary = Column(Integer)
    date_next_salary_increase = Column(Date)
