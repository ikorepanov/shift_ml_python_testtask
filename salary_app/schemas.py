"""
schemas.py

This module contains the Pydantic models used for representing user data
and tokens in the Salary App.The models define the structure and validation
rules for the data used in the application.
"""

from datetime import date, datetime
from typing import Optional, Union

from pydantic import BaseModel


class UserBase(BaseModel):
    """
    Base model for user data.
    """

    username: str
    current_salary: int
    date_next_salary_increase: date


class UserCreate(UserBase):
    """
    Model for creating a new user.
    Inherits from UserBase and adds a password field.
    """

    password: str


class User(UserBase):
    """
    Model for user data.
    Inherits from UserBase and adds an ID field.
    """

    id: int

    class Config:
        orm_mode = True


class Token(BaseModel):
    """
    Model for JWT token data.
    Used in the token endpoint for the response.
    """

    access_token: str
    token_type: str


class TokenData(BaseModel):
    """
    Model for token data.
    Contains the username and expiration date of a token.
    """

    username: Union[str, None] = None
    expires: Optional[datetime]
