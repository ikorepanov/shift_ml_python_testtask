"""
crud.py

This module provides CRUD operations for the database.
"""

from sqlalchemy.orm import Session

from salary_app import models, schemas, utils


def get_user_by_username(db: Session, username: str) -> models.User:
    """Retrieves a user from the database based on the username."""
    return (
        db.query(models.User).filter(models.User.username == username).first()
    )


def create_user(db: Session, user: schemas.UserCreate) -> schemas.User:
    """Creates a new user in the database."""
    hashed_password = utils.get_password_hash(user.password)
    db_user = models.User(
        username=user.username,
        hashed_password=hashed_password,
        current_salary=user.current_salary,
        date_next_salary_increase=user.date_next_salary_increase
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user
