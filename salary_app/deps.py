"""
deps.py

This module provides dependency functions used in FastAPI application.
"""

from typing import Annotated, Generator

from fastapi import Depends, HTTPException, status
from jose import ExpiredSignatureError, JWTError, jwt
from sqlalchemy.orm import Session

from salary_app import crud, schemas, utils
from salary_app.database import SessionLocal


def get_db() -> Generator[Session, None, None]:
    """
    Creates a new SQLAlchemy SessionLocal that will be used in a single
    request, and then closes it once the request is finished.
    """
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


async def get_current_user(token: Annotated[str, Depends(utils.oauth2_scheme)],
                           db: Session = Depends(get_db)) -> schemas.User:
    """
    Gets the current user based on the provided token.
    """
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail='Could not validate credentials',
        headers={'WWW-Authenticate': 'Bearer'},
    )
    try:
        payload = jwt.decode(token,
                             utils.SECRET_KEY,
                             algorithms=[utils.ALGORITHM])
        username: str = payload.get('sub')
        if username is None:
            raise credentials_exception
        expires: int = payload.get('exp')
        if expires is None:
            raise credentials_exception
        token_data = schemas.TokenData(username=username, expires=expires)
    except ExpiredSignatureError:
        raise HTTPException(status_code=403, detail='Token has been expired')
    except JWTError:
        raise credentials_exception
    user = crud.get_user_by_username(db, username=token_data.username)
    if user is None:
        raise credentials_exception
    return user
