"""
Unit tests for the 'main' module.

This module contains unit tests for the main functionality and routes
of the application.
The tests focus on verifying the behavior of the API endpoints,
request handling, and response formats.

The tests are written using the pytest framework and utilize the 'TestClient'
from 'fastapi.testclient' to simulate HTTP requests and assert the expected
responses.

To run the tests, execute 'pytest' in the project root directory.
"""

from datetime import timedelta
from typing import Any, Dict, Generator

import pytest
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from salary_app.database import Base
from salary_app.deps import get_db
from salary_app.main import app
from salary_app.utils import create_access_token

SQLALCHEMY_DATABASE_URL = 'sqlite:///./db_test.sqlite3'

engine = create_engine(SQLALCHEMY_DATABASE_URL,
                       connect_args={'check_same_thread': False})
TestingSessionLocal = sessionmaker(autocommit=False,
                                   autoflush=False,
                                   bind=engine)


def override_get_db() -> Generator:
    """Override the `get_db` dependency to use a test database session."""
    db = TestingSessionLocal()
    try:
        yield db
    finally:
        db.close()


app.dependency_overrides[get_db] = override_get_db

client = TestClient(app)


@pytest.fixture()
def test_db() -> Generator:
    """Fixture for creating and dropping the test database."""
    Base.metadata.create_all(bind=engine)
    yield
    Base.metadata.drop_all(bind=engine)


@pytest.fixture()
def registered_user() -> Dict[str, Any]:
    """Fixture for registering a test user and returning the user data."""
    register_data: Dict[str, Any] = {
        'username': 'testuser',
        'current_salary': 1000,
        'date_next_salary_increase': '2024-07-07',
        'password': 'secret'
    }
    response = client.post('/signup', json=register_data)
    return response.json()


def test_create_user(test_db: None, registered_user: Dict[str, Any]) -> None:
    """Test case for creating a user and verifying the response data."""
    assert registered_user['username'] == 'testuser'
    assert registered_user['current_salary'] == 1000
    assert registered_user['date_next_salary_increase'] == '2024-07-07'
    assert 'id' in registered_user


def test_create_user_same_username(test_db: None,
                                   registered_user: Dict[str, Any]) -> None:
    """
    Test case for attempting to create a user with an already
    registered username.
    """
    register_data_same_username: Dict[str, Any] = {
        'username': 'testuser',
        'current_salary': 2000,
        'date_next_salary_increase': '2025-07-07',
        'password': 'anothersecret'
    }
    response = client.post('/signup', json=register_data_same_username)
    assert response.status_code == 409
    assert response.json()['detail'] == 'Username already registered'


def test_get_token(test_db: None, registered_user: Dict[str, Any]) -> None:
    """Test case for obtaining an access token with valid credentials."""
    login_data: Dict[str, str] = {
        'username': 'testuser',
        'password': 'secret'
    }
    response = client.post('/login', data=login_data)
    assert response.status_code == 200
    access_token: str = response.json()['access_token']
    assert access_token is not None


def test_get_token_incorrect_username(test_db: None,
                                      registered_user: Dict[str, Any]) -> None:
    """
    Test case for attempting to obtain an access token with incorrect username.
    """
    login_data_incorrect_password: Dict[str, str] = {
        'username': 'testuser',
        'password': 'notasecret'
    }
    response = client.post('/login', data=login_data_incorrect_password)
    assert response.status_code == 401
    assert response.json()['detail'] == 'Incorrect username or password'


def test_get_personal_information(test_db: None,
                                  registered_user: Dict[str, Any]) -> None:
    """
    Test case for retrieving personal information of an authenticated user.
    """
    login_data: Dict[str, str] = {
        'username': 'testuser',
        'password': 'secret'
    }
    response = client.post('/login', data=login_data)
    access_token: str = response.json()['access_token']
    response = client.get("/users/me",
                          headers={'Authorization': f'Bearer {access_token}'})
    assert response.status_code == 200
    assert response.json() == registered_user


def test_get_personal_information_without_bearer(
        test_db: None, registered_user: Dict[str, Any]
        ) -> None:
    """
    Test case for attempting to retrieve personal information without
    the Bearer token.
    """
    login_data: Dict[str, str] = {
        'username': 'testuser',
        'password': 'secret'
    }
    response = client.post('/login', data=login_data)
    access_token: str = response.json()['access_token']
    response = client.get("/users/me",
                          headers={'Authorization': f'{access_token}'})
    assert response.status_code == 401
    assert response.json()['detail'] == 'Not authenticated'


def test_get_personal_information_invalid_token(
        test_db: None, registered_user: Dict[str, Any]
        ) -> None:
    """
    Test case for attempting to retrieve personal information with
    an invalid token.
    """
    response = client.get("/users/me",
                          headers={'Authorization': 'Bearer Invalid token'})
    assert response.status_code == 401
    assert response.json()['detail'] == 'Could not validate credentials'


def test_get_personal_information_expired_token(
        test_db: None, registered_user: Dict[str, Any]
        ) -> None:
    """
    Test case for attempting to retrieve personal information with
    an expired token.
    """
    username: str = registered_user['username']
    access_token: str = create_access_token(
        data={'sub': username},
        expires_delta=timedelta(minutes=-15)
        )
    response = client.get("/users/me",
                          headers={'Authorization': f'Bearer {access_token}'})
    assert response.status_code == 403
    assert response.json()['detail'] == 'Token has been expired'
