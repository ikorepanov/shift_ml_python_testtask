"""Package containing unit tests for the application.

This package contains a collection of unit tests that verify the functionality
and behavior of the application.
Each test module within the package focuses on specific components or features
of the application.

Modules:
- 'test_main.py': contains tests for module 'main'.

Test cases are written using the pytest framework and can be executed
by running `pytest` in the project root directory.
"""
