# Salary App
## Solution of the test task on the course "Python", SHIFT, CFT
### Table of Contents
- [Description](#description)
  - [Endpoints](#endpoints)
  - [Directory Structure](#directory-structure)
  - [Technologies](#technologies)
- [Getting Started](#getting-started)
- [Usage](#usage)
  - [POST: http://localhost:8000/signup](#post-httplocalhost8000signup) 
    - [200 OK](#200-ok)
    - [409 Conflict](#409-conflict)
  - [POST: http://localhost:8000/login](#post-httplocalhost8000login)
    - [200 OK](#200-ok-1)
    - [401 Unauthorized](#401-unauthorized)
  - [GET: http://localhost:8000/users/me](#get-httplocalhost8000usersme)
    - [200 OK](#200-ok-2)
    - [401 Unauthorized (request without a word “Bearer” in the header)](#401-unauthorized-request-without-a-word-bearer-in-the-header)
    - [401 Unauthorized (request with invalid token)](#401-unauthorized-request-with-invalid-token)
    - [403 Forbidden (request with an expired token)](#403-forbidden-request-with-an-expired-token)
- [Tests](#tests)
- [Documentation](#documentation)
- [Author](#author)

### Description
Implementation of a REST service for viewing the current salary and the date of the next one raises. Each employee can only see their own amount. To ensure security, a method is implemented where a secret token is issued using the employee's login and password, which is valid for a certain time. A response to a request for salary data is issued only upon presentation of a valid token.
- Implemented 1 User model and 3 endpoints;
- Dependencies are fixed by the dependency manager Poetry;
- Tests written using Pytest.

##### Endpoints:
- **/signup** - creating a user by providing information about the user's current salary, next salary increase date, and username and password. The hashed version of the password is stored in the database;
- **/login** - logging in, getting access- and refresh- tokens by username and password;
- **/user/me** - viewing personal user data: for authenticated users.

##### Directory Structure
```
├── salary_app
│   ├── config.py
│   ├── crud.py
│   ├── database.py
│   │── deps.py
│   │── main.py
│   ├── models.py
│   │── schemas.py
│   ├── utils.py
│   ├── __init__.py
│   └── tests
│       ├── __init__.py
│       └── test_main.py
├── .env.example
├── .gitignore
├── README.md
└── requirements.txt
```
##### Technologies
- Python 3.9
- FastAPI 0.98.0
- SQLAlchemy 2.0.17
- Pydantic 1.10.9
- Python-jose 3.3.0
- Passlib 1.7.4
- Poetry 1.5.1
- Pytest 7.4.0

### Getting Started
- Clone the repository:
```
git clone git@gitlab.com:ikorepanov/shift_ml_python_testtask.git
```
###### or
```
git clone https://gitlab.com/ikorepanov/shift_ml_python_testtask.git
```
- Make sure Poetry is installed on your machine. If not use for Windows:
```
(Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -
```
###### or
for Unix-systems:
```
curl -sSL https://install.python-poetry.org | python3 -
```
**NB!** For more info consider visiting https://python-poetry.org/docs/
- Add Poetry to your PATH using this guide:
```
https://linuxhint.com/add-directory-to-path-environment-variables-windows/
``` 
- Go to the project directory:
```
cd shift_ml_python_testtask
```
- Install dependencies:
```
poetry install
```
- Duplicate the '.env.example' file in the same directory, rename the copy to '.env':
```
cp .env.example .env
```
- fill in the variables listed in the file with the provided data examples:
```
nano .env
```
- Create a virtual environment and run a shell with the virtual environment enabled:
```
poetry shell
```
**NB!** Virtual environment will be created in {cache_dir}/virtualenvs.
- start the project in a development mode:
```
uvicorn salary_app.main:app --reload
```
**NB!** Database file db.sqlite3 will be automatically created in the main directory of the project after the app starts.
### Usage
##### POST: http://localhost:8000/signup
##### 200 OK
###### REQUEST

```JSON
{
  "username": "John",
  "current_salary": 1000,
  "date_next_salary_increase": "2024-07-03",
  "password": "secret"
}
```
###### RESPONSE
```JSON
{
    "username": "John",
    "current_salary": 1000,
    "date_next_salary_increase": "2024-07-03",
    "id": 1
}
```
##### 409 Conflict
###### REQUEST
```JSON
{
  "username": "John",
  "current_salary": 2000,
  "date_next_salary_increase": "2034-07-03",
  "password": "secret"
}
```
###### RESPONSE
```JSON
{
    "detail": "Username already registered"
}
```
##### POST: http://localhost:8000/login
##### 200 OK
###### REQUEST
**NB!** Username and password are provided via form-data:
| Key       | Value        |
|---------  |--------------|
| username  |         John |
| password  |       secret |
###### RESPONSE
```JSON
{
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJKaG9uIiwiZXhwIjoxNjg4MzU5ODI5fQ.B9vm6xDkYhHSGRR4sBAK5G5SyMxnzqazgBtMQhbcks0",
    "token_type": "bearer"
}
```
##### 401 Unauthorized
###### REQUEST
**NB!** Username and password are provided via form-data:
| Key       | Value        |
|---------  |--------------|
| username  |         John |
| password  |   notasecret |
###### RESPONSE
```JSON
{
    "detail": "Incorrect username or password"
}
```
##### GET: http://localhost:8000/users/me
##### 200 OK
###### REQUEST
**NB!** The request must include an "Authorization" header with a word "Bearer" and JWT-token
| Header           | Value                        |
|------------------|------------------------------|
| Authorization    | Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c |
```JSON
{
  "username": "John",
  "password": "secret"
}
```
###### RESPONSE
```JSON
{
    "username": "John",
    "current_salary": 1000,
    "date_next_salary_increase": "2024-07-03",
    "id": 1
}
```
##### 401 Unauthorized (request without a word "Bearer" in the header)
###### REQUEST
| Header           | Value                        |
|------------------|------------------------------|
| Authorization    | eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c                       |
###### RESPONSE
```JSON
{
    "detail": "Not authenticated"
}
```
##### 401 Unauthorized (request with invalid token)
###### REQUEST
| Header           | Value                        |
|------------------|------------------------------|
| Authorization    | Bearer Invalid token         |
###### RESPONSE
```JSON
{
    "detail": "Could not validate credentials"
}
```
##### 403 Forbidden (request with an expired token)
###### REQUEST
| Header           | Value                        |
|------------------|------------------------------|
| Authorization    | Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c |
###### RESPONSE
```JSON
{
    "detail": "Token has been expired"
}
```
### Tests
All of the above scenarios are tested using paytest. To run tests:
```
pytest
```
After starting testing, a file db_test.sqlite3 is automatically created in the root directory.
### Documentation
You can find documentation automatically generated by FastAPI on https://localhost:8000/docs.
### Author
Ilia Korepanov, https://t.me/number_one_lobster
### P.S.:
I got a great experience and a lot of fun doing this task. 
I figured out how FastApi and the authentication system work, I learned about Poetry. 
Thank you for the opportunity to do this work.
